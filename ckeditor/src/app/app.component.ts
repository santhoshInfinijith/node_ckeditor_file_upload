import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ckeditor';
  public editorValue = '';
  public config: any = {
    removeDialogTabs: 'image:advanced;image:Link',
    filebrowserBrowseUrl: 'http://localhost:9000/files',
    filebrowserImageUploadUrl: 'http://localhost:3000/upload',
    filebrowserUploadMethod: 'xhr',
    toolbar: [
      { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
      { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat'] },
      '/',
      { name: 'editing', items: ['SpellChecker'] },
      { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'Iframe'] },
      { name: 'paragraph', items: ['NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
      { name: 'colors', items: ['TextColor', 'BGColor'] },
      '/',
      { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
      { name: 'tools', items: ['Maximize', 'ShowBlocks', 'Save'] }
    ]
  };

  constructor() { }
}
