const express = require('express');
const fileUpload = require('express-fileupload');
var cors = require('cors');
const app = express();
var whitelist = ['http://evil.com/'];

 var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}
// default options
app.use(fileUpload());
 
app.post('/upload', cors(corsOptions), function(req, res) {
  if (!req.files)
    return res.status(400).send('No files were uploaded.');
 
  let sampleFile = req.files.upload;
 
  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv('D:\/santhosh\/fileserver\/files\/'+sampleFile.name, function(err) {
    if (err)
      return res.status(500).send(err);
    var json = {
        "fileName":sampleFile.name,
        "uploaded":1,
        "url":"http://localhost:9000/files/"+sampleFile.name
    };
    res.status(200).send(json);
  });
});

app.listen(3000, function(){
  console.log('Server listening on port 3000');
});